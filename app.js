var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');

var hostname = 'localhost';
var port = 3000;

// Require dishes router
var dishes = require('./routes/dishes');

var app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(express.static(__dirname+'/public'));

app.use('/dishes', dishes);

app.listen(port, hostname, function(){
  console.log(`Serverr running at http://${hostname}:${port}`);
});
