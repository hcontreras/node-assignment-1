var express = require('express');
var router = express.Router();

router.get('/newRouter', function(req, res, next){
  res.writeHead(200, { 'Content-Type': 'text/html' });
  res.end('Accessing new Router');
});

module.exports = router;
